# Limit : Under this limit, the external API is not consumed and
# local data is returned.
REFRESH_DATA_LIMIT=15*60


OPENWEATHER_API_URL_TEMPLATE="http://api.openweathermap.org/data/2.5/weather?zip={zip_code},{country_code}&appid={api_key}"
OPENWEATHER_ICON_URL_TEMPLATE="http://openweathermap.org/img/wn/{icon}@2x.png"
ZIP_CODE_LENGTH=5
