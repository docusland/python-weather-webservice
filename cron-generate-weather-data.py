"""
This script is to be executed by a cron task.
Querying a given route on which the main script of this repository is running.
"""

import requests
from dotenv import load_dotenv
import os
load_dotenv()
ZIPCODES = os.environ.get('ZIPCODES_FILE')
with open(ZIPCODES, 'r', encoding = 'utf-8') as f:
    for zip in f:
        URL=os.environ.get('WEATHER_URL_TEMPLATE')
        address = URL.format(zipcode=zip.strip())
        try:
            response = requests.head(address, timeout=20)
            if response.status_code >= 400:
                print("Website %s returned status_code=%s" % (address, response.status_code))
            else:
                print('OK for ' + address)
        except requests.exceptions.RequestException:
            print("Timeout expired for website %s" % address)

